package com.app.compras.resources;


import java.util.List;

import com.app.compras.resources.utils.URLUtils;
import com.app.compras.vo.CategoryVO;
import com.app.compras.vo.ProductVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.compras.domain.Product;
import com.app.compras.services.ProductService;


@RestController
@RequestMapping( value = "/products" )
public class ProductResource {

    @Autowired
    private ProductService productService;


//    @RequestMapping( method = RequestMethod.GET )
//    public List< Product > listAll() {
//
//        // Category cat = new Category( 1, "Informática" );
//        // Category cat2 = new Category( 2, "Escritório" );
//
//        // List< Category > Products = new ArrayList<>();
//        // Products.addAll( Arrays.asList( cat, cat2 ) );
//
//        List< Product > products = productService.listAll();
//
//        return products;
//    }


    @RequestMapping( value = "/{id}", method = RequestMethod.GET )
    public ResponseEntity< ? > findById( @PathVariable Integer id ) {

        Product product = productService.findById( id );
        return ResponseEntity.ok().body( product );
    }


    @RequestMapping( method = RequestMethod.GET )
    public ResponseEntity< Page< ProductVO > > findPage(
        @RequestParam( value = "name", defaultValue = "" ) String name,
        @RequestParam( value = "categories", defaultValue = "" ) String categories,
        @RequestParam( value = "page", defaultValue = "0" ) Integer page,
        @RequestParam( value = "linesPerPage", defaultValue = "24" ) Integer linesPerPage,
        @RequestParam( value = "orderBy", defaultValue = "name" ) String orderBy,
        @RequestParam( value = "direction", defaultValue = "ASC" ) String direction ) {

        List< Integer > ids = URLUtils.decodeIntList( categories );
        String checkedName = URLUtils.decodeParam( name );

        Page< Product > pages = productService.search( checkedName, ids, page, linesPerPage, orderBy, direction );
        Page< ProductVO > productVOS = pages.map( obj -> new ProductVO( obj ) );
        return ResponseEntity.ok().body( productVOS );
    }

}
