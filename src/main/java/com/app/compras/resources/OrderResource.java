package com.app.compras.resources;


import java.util.List;

import com.app.compras.domain.Order;
import com.app.compras.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.app.compras.domain.Category;
import com.app.compras.services.CategoryService;


@RestController
@RequestMapping( value = "/pedidos" )
public class OrderResource {

    @Autowired
    private OrderService orderService;


    @RequestMapping( method = RequestMethod.GET )
    public List< Order > listAll() {

        // Category cat = new Category( 1, "Informática" );
        // Category cat2 = new Category( 2, "Escritório" );

        // List< Category > orders = new ArrayList<>();
        // orders.addAll( Arrays.asList( cat, cat2 ) );

        List< Order > orders = orderService.listAll();

        return orders;
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.GET )
    public ResponseEntity< ? > findById( @PathVariable Integer id ) {

        Order order = orderService.findById( id );
        return ResponseEntity.ok().body( order );
    }

}
