package com.app.compras.resources;


import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import com.app.compras.vo.CategoryVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.app.compras.domain.Category;
import com.app.compras.services.CategoryService;

import javax.validation.Valid;


@RestController
@RequestMapping( value = "/categorias" )
public class CategoryResource {

    @Autowired
    private CategoryService categoryService;


    @RequestMapping( method = RequestMethod.GET )
    public ResponseEntity< List< CategoryVO > > listAll() {

        List< Category > categories = categoryService.listAll();
        List< CategoryVO > categoriesVO = categories.stream().map( obj -> new CategoryVO( obj ) ).collect( Collectors.toList() );

        return ResponseEntity.ok().body( categoriesVO );
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.GET )
    public ResponseEntity< ? > findById( @PathVariable Integer id ) {

        Category category = categoryService.findById( id );
        return ResponseEntity.ok().body( category );
    }


    @RequestMapping( method = RequestMethod.POST )
    public ResponseEntity< Void > addCategory( @Valid @RequestBody CategoryVO categoryVO ) {

        Category category = categoryService.convertVO( categoryVO );

        category = categoryService.insert( category );

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path( "/{id}" ).buildAndExpand( category.getId() ).toUri();

        return ResponseEntity.created( uri ).build();
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.PUT )
    public ResponseEntity< Void > updateCategory(@Valid @RequestBody CategoryVO categoryVO, @PathVariable Integer id ) {

        Category category = categoryService.convertVO( categoryVO );
        category.setId( id );
        category = categoryService.update( category );

        return ResponseEntity.noContent().build();
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE )
    public ResponseEntity< Void > deleteCategory( @PathVariable Integer id ) {

        categoryService.delete( id );
        return ResponseEntity.noContent().build();
    }


    @RequestMapping( value = "/page", method = RequestMethod.GET )
    public ResponseEntity< Page > findPage(
        @RequestParam( value = "page", defaultValue = "0" ) Integer page,
        @RequestParam( value = "linesPerPage", defaultValue = "24" ) Integer linesPerPage,
        @RequestParam( value = "orderBy", defaultValue = "name" ) String orderBy,
        @RequestParam( value = "direction", defaultValue = "ASC" ) String direction ) {

        Page< Category > pages = categoryService.findPage( page, linesPerPage, orderBy, direction );
        Page< CategoryVO > categoriesVO = pages.map( obj -> new CategoryVO( obj ) );
        return ResponseEntity.ok().body( categoriesVO );
    }

}
