package com.app.compras.resources.utils;


import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class URLUtils {

    public static List< Integer > decodeIntList( String val ) {

        return Arrays.asList( val.split( "," ) ).stream().map( x -> Integer.parseInt( x ) ).collect( Collectors.toList() );
    }


    public static String decodeParam( String param ) {

        try {
            return URLDecoder.decode( param, "UTF-8" );
        } catch ( UnsupportedEncodingException e ) {
            return "";
        }
    }
}
