package com.app.compras.resources;


import java.net.URI;
import java.util.List;

import com.app.compras.domain.Client;
import com.app.compras.domain.Client;
import com.app.compras.services.ClientService;
import com.app.compras.vo.ClientNewVO;
import com.app.compras.vo.ClientVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;


@RestController
@RequestMapping( value = "/clientes" )
public class ClientResource {

    @Autowired
    private ClientService clientService;


    @RequestMapping( method = RequestMethod.GET )
    public List< Client > listAll() {

        // Client cat = new Client( 1, "Informática" );
        // Client cat2 = new Client( 2, "Escritório" );

        // List< Client > categories = new ArrayList<>();
        // categories.addAll( Arrays.asList( cat, cat2 ) );

        List< Client > clients = clientService.listAll();

        return clients;
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.GET )
    public ResponseEntity< ? > findById( @PathVariable Integer id ) {

        Client client = clientService.findById( id );
        return ResponseEntity.ok().body( client );
    }


    @RequestMapping( method = RequestMethod.POST )
    public ResponseEntity< Void > addClient( @Valid @RequestBody ClientNewVO clientNewVO ) {

        Client client = clientService.convertVO( clientNewVO );

        client = clientService.insert( client );

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path( "/{id}" ).buildAndExpand( client.getId() ).toUri();

        return ResponseEntity.created( uri ).build();
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.PUT )
    public ResponseEntity< Void > updateClient( @Valid @RequestBody ClientVO clientVO, @PathVariable Integer id ) {

        Client client = clientService.convertVO( clientVO );
        client.setId( id );
        client = clientService.update( client );

        return ResponseEntity.noContent().build();
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE )
    public ResponseEntity< Void > deleteClient( @PathVariable Integer id ) {

        clientService.delete( id );
        return ResponseEntity.noContent().build();
    }


    @RequestMapping( value = "/page", method = RequestMethod.GET )
    public ResponseEntity< Page > findPage(
        @RequestParam( value = "page", defaultValue = "0" ) Integer page,
        @RequestParam( value = "linesPerPage", defaultValue = "24" ) Integer linesPerPage,
        @RequestParam( value = "orderBy", defaultValue = "name" ) String orderBy,
        @RequestParam( value = "direction", defaultValue = "ASC" ) String direction ) {

        Page< Client > pages = clientService.findPage( page, linesPerPage, orderBy, direction );
        Page< ClientVO > categoriesVO = pages.map( obj -> new ClientVO( obj ) );
        return ResponseEntity.ok().body( categoriesVO );
    }

}
