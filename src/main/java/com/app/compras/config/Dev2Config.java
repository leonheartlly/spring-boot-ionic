package com.app.compras.config;


import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.app.compras.services.DBService;


@Configuration
@Profile( "dev2" )
public class Dev2Config {

    @Autowired
    private DBService dbService;

    @Value( "${spring.jpa.hibernate.ddl-auto}" )
    private String databaseConf;


    @Bean
    public boolean instanteateDataBase()
        throws ParseException {

        if ( !"create".equals( databaseConf ) )
            return false;

        dbService.instantiateDevDataBase();
        return true;
    }
}
