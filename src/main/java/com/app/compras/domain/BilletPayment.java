package com.app.compras.domain;


import com.app.compras.domain.enuns.PaymentState;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import java.util.Date;


@Entity
public class BilletPayment extends Payment {

    @JsonFormat( pattern = "dd/MM/yyyy" )
    private Date endingDate;

    @JsonFormat( pattern = "dd/MM/yyyy" )
    private Date paymentDate;


    public BilletPayment( Integer id, PaymentState paymentState, Order order, Date endingDate, Date paymentDate ) {

        super( id, paymentState, order );
        this.endingDate = endingDate;
        this.paymentDate = paymentDate;
    }


    public BilletPayment() {

    }


    public Date getEndingDate() {

        return endingDate;
    }


    public void setEndingDate( Date endingDate ) {

        this.endingDate = endingDate;
    }


    public Date getPaymentDate() {

        return paymentDate;
    }


    public void setPaymentDate( Date paymentDate ) {

        this.paymentDate = paymentDate;
    }
}
