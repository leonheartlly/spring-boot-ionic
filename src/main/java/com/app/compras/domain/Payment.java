package com.app.compras.domain;


import com.app.compras.domain.enuns.PaymentState;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import java.io.Serializable;
import java.util.Objects;


@Entity
@Inheritance( strategy = InheritanceType.JOINED )
public abstract class Payment implements Serializable {

    @Id
    private Integer id;

    private Integer paymentState;

    @JsonIgnore
    @OneToOne
    @JoinColumn( name = "order_id" )
    @MapsId
    private Order order;


    public Payment( Integer id, PaymentState paymentState, Order order ) {

        this.id = id;
        this.paymentState = ( paymentState == null ) ? null : paymentState.getCod();
        this.order = order;
    }


    public Payment() {

    }


    public Integer getId() {

        return id;
    }


    public void setId( Integer id ) {

        this.id = id;
    }


    public PaymentState getPaymentState() {

        return PaymentState.getPaymentState( this.paymentState );
    }


    public void setPaymentState( PaymentState paymentState ) {

        this.paymentState = paymentState.getCod();
    }


    public Order getOrder() {

        return order;
    }


    public void setOrder( Order order ) {

        this.order = order;
    }


    @Override
    public boolean equals( Object o ) {

        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        Payment payment = (Payment) o;
        return id.equals( payment.id );
    }


    @Override
    public int hashCode() {

        return Objects.hash( id );
    }
}
