package com.app.compras.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Objects;


@Entity
public class OrderItem implements Serializable {

    @JsonIgnore
    @EmbeddedId
    private OrderItemPK id = new OrderItemPK();

    private Double discount;

    private Integer quantity;

    private Double price;


    public OrderItem( Order order, Product product, Double discount, Integer quantity, Double price ) {

        this.id.setOrder( order );
        this.id.setProduct( product );
        this.discount = discount;
        this.quantity = quantity;
        this.price = price;
    }


    public OrderItem() {

    }


    @JsonIgnore
    public Order getOrder() {

        return this.id.getOrder();
    }


    public void setOrder( Order order ) {

        this.id.setOrder( order );
    }


    public Product getProduct() {

        return this.id.getProduct();
    }


    public void setProduct( Product product ) {

        this.id.setProduct( product );
    }


    public OrderItemPK getId() {

        return id;
    }


    public void setId( OrderItemPK id ) {

        this.id = id;
    }


    public Double getDiscount() {

        return discount;
    }


    public void setDiscount( Double discount ) {

        this.discount = discount;
    }


    public Integer getQuantity() {

        return quantity;
    }


    public void setQuantity( Integer quantity ) {

        this.quantity = quantity;
    }


    public Double getPrice() {

        return price;
    }


    public void setPrice( Double price ) {

        this.price = price;
    }


    @Override
    public boolean equals( Object o ) {

        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        OrderItem orderItem = (OrderItem) o;
        return id.equals( orderItem.id );
    }


    @Override
    public int hashCode() {

        return Objects.hash( id );
    }
}
