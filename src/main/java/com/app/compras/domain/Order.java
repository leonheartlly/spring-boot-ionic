package com.app.compras.domain;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


@Entity
@Table( name = "TB_ORDER" )
public class Order implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Integer id;

    @JsonFormat(pattern="dd/MM/yyyy HH:mm")
    private Date date;

    @OneToOne( cascade = CascadeType.ALL, mappedBy = "order" )
    private Payment payment;

    @ManyToOne
    @JoinColumn( name = "client_id" )
    private Client client;

    @ManyToOne
    @JoinColumn( name = "delivery_address_id" )
    private Address address;

    @OneToMany( mappedBy = "id.order" )
    private Set< OrderItem > itens = new HashSet<>();


    public Order( Integer id, Date date, Client client, Address address ) {

        this.id = id;
        this.date = date;
        this.client = client;
        this.address = address;
    }


    public Order() {

    }


    public Integer getId() {

        return id;
    }


    public void setId( Integer id ) {

        this.id = id;
    }


    public Date getDate() {

        return date;
    }


    public void setDate( Date date ) {

        this.date = date;
    }


    public Payment getPayment() {

        return payment;
    }


    public void setPayment( Payment payment ) {

        this.payment = payment;
    }


    public Client getClient() {

        return client;
    }


    public void setClient( Client client ) {

        this.client = client;
    }


    public Address getAddress() {

        return address;
    }


    public void setAddress( Address address ) {

        this.address = address;
    }


    public Set< OrderItem > getItens() {

        return itens;
    }


    public void setItens( Set< OrderItem > itens ) {

        this.itens = itens;
    }


    @Override
    public boolean equals( Object o ) {

        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        Order order = (Order) o;
        return id.equals( order.id );
    }


    @Override
    public int hashCode() {

        return Objects.hash( id );
    }
}
