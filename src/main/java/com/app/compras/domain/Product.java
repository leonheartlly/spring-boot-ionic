package com.app.compras.domain;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;


@Entity
public class Product implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Integer id;

    private String name;

    private Double price;

    @JsonIgnore
    @ManyToMany
    @JoinTable( name = "PRODUCT_CATEGORY", joinColumns = @JoinColumn( name = "product_id" ), inverseJoinColumns = @JoinColumn( name = "category_id" ) )
    private List< Category > categories = new ArrayList<>();

    @JsonIgnore
    @OneToMany( mappedBy = "id.product" )
    private Set< OrderItem > itens = new HashSet<>();


    public Product( Integer id, String name, Double price ) {

        this.id = id;
        this.name = name;
        this.price = price;
    }


    @JsonIgnore // tudo que é começado com get é automaticamente serializado, usado jsonignore
                // aqui por isso
    public List< Order > getOrders() {

        List< Order > list = new ArrayList<>();
        for ( OrderItem oi : itens ) {
            list.add( oi.getOrder() );
        }
        return list;
    }


    public Product() {

    }


    public Integer getId() {

        return id;
    }


    public void setId( Integer id ) {

        this.id = id;
    }


    public String getName() {

        return name;
    }


    public void setName( String name ) {

        this.name = name;
    }


    public Double getPrice() {

        return price;
    }


    public void setPrice( Double price ) {

        this.price = price;
    }


    public List< Category > getCategories() {

        return categories;
    }


    public void setCategories( List< Category > categories ) {

        this.categories = categories;
    }


    public Set< OrderItem > getItens() {

        return itens;
    }


    public void setItens( Set< OrderItem > itens ) {

        this.itens = itens;
    }


    @Override
    public boolean equals( Object o ) {

        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        Product product = (Product) o;
        return id.equals( product.id );
    }


    @Override
    public int hashCode() {

        return Objects.hash( id );
    }
}
