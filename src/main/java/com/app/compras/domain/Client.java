package com.app.compras.domain;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.app.compras.domain.enuns.ClientType;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Client implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Integer id;

    private String name;

    @Column( unique = true )
    private String email;

    private String cpfcnpj;

    private Integer clientType;

    @OneToMany( mappedBy = "client", cascade = CascadeType.ALL )
    private List< Address > addresses = new ArrayList<>();

    @ElementCollection
    @CollectionTable( name = "PHONE" )
    private Set< String > phones = new HashSet<>();

    @JsonIgnore
    @OneToMany( mappedBy = "client" )
    private List< Order > orders = new ArrayList<>();


    public Client( Integer id, String name, String email, String cpfcnpj, ClientType clientType ) {

        this.id = id;
        this.name = name;
        this.email = email;
        this.cpfcnpj = cpfcnpj;
        this.clientType = ( clientType == null ) ? null : clientType.getCod();
    }


    public Client() {

    }


    public Integer getId() {

        return id;
    }


    public void setId( Integer id ) {

        this.id = id;
    }


    public String getName() {

        return name;
    }


    public void setName( String name ) {

        this.name = name;
    }


    public String getEmail() {

        return email;
    }


    public void setEmail( String email ) {

        this.email = email;
    }


    public String getCpfcnpj() {

        return cpfcnpj;
    }


    public void setCpfcnpj( String cpfcnpj ) {

        this.cpfcnpj = cpfcnpj;
    }


    public ClientType getClientType() {

        return ClientType.getClientType( this.clientType );
    }


    public void setClientType( ClientType clientType ) {

        this.clientType = clientType.getCod();
    }


    public List< Address > getAddresses() {

        return addresses;
    }


    public void setAddresses( List< Address > addresses ) {

        this.addresses = addresses;
    }


    public Set< String > getPhones() {

        return phones;
    }


    public void setPhones( Set< String > phones ) {

        this.phones = phones;
    }


    public List< Order > getOrders() {

        return orders;
    }


    public void setOrders( List< Order > orders ) {

        this.orders = orders;
    }


    @Override
    public boolean equals( Object o ) {

        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        Client client = (Client) o;
        return id.equals( client.id );
    }


    @Override
    public int hashCode() {

        return Objects.hash( id );
    }
}
