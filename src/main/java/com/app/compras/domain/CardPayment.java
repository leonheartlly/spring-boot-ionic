package com.app.compras.domain;


import com.app.compras.domain.enuns.PaymentState;

import javax.persistence.Entity;


@Entity
public class CardPayment extends Payment {

    private Integer cardPortion;


    public CardPayment( Integer id, PaymentState paymentState, Order order, Integer cardPortion ) {

        super( id, paymentState, order );
        this.cardPortion = cardPortion;
    }


    public CardPayment() {

    }


    public Integer getCardPortion() {

        return cardPortion;
    }


    public void setCardPortion( Integer cardPortion ) {

        this.cardPortion = cardPortion;
    }
}
