package com.app.compras.domain.enuns;


import java.util.Optional;


public enum PaymentState {

    PENDENTE( 1, "Pendente" ),
    QUITADO( 2, "Quitado" ),
    CANCELADO( 3, "Cancelado" );

    private int cod;

    private String desc;


    PaymentState( int cod, String desc ) {

        this.cod = cod;
        this.desc = desc;
    }


    public int getCod() {

        return cod;
    }


    public void setCod( int cod ) {

        this.cod = cod;
    }


    public String getDesc() {

        return desc;
    }


    public void setDesc( String desc ) {

        this.desc = desc;
    }


    public static PaymentState getPaymentState( Integer cod ) {

        if ( !Optional.ofNullable( cod ).isPresent() ) {
            return null;
        }

        for ( PaymentState c : PaymentState.values() ) {
            if ( cod.equals( c.getCod() ) ) {
                return c;
            }
        }
        throw new IllegalArgumentException( "Id inválido: " + cod );
    }
}
