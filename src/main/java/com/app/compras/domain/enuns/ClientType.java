package com.app.compras.domain.enuns;


import java.util.Optional;


public enum ClientType {

    PESSOA_FISICA( 1, "Pessoa Física" ),
    PESSOA_JURIDICA( 2, "Pessoa Jurídica" );

    private int cod;

    private String desc;


    private ClientType( int cod, String desc ) {

        this.cod = cod;
        this.desc = desc;
    }


    public int getCod() {

        return cod;
    }


    public String getDesc() {

        return desc;
    }


    public static ClientType getClientType( Integer cod ) {

        if ( !Optional.ofNullable( cod ).isPresent() ) {
            return null;
        }

        for ( ClientType c : ClientType.values() ) {
            if ( cod.equals( c.getCod() ) ) {
                return c;
            }
        }
        throw new IllegalArgumentException( "Id inválido: " + cod );
    }
}
