package com.app.compras.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.compras.domain.Client;


@Repository
public interface ClientRepository extends JpaRepository< Client, Integer > {

    @Transactional( readOnly = true ) //deixa a transação mais rapida e diminui o locking de transação
    Client findByEmail( String email );
}
