package com.app.compras.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.compras.domain.Order;


@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {

}
