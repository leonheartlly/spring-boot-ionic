package com.app.compras.repositories;


import com.app.compras.domain.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.compras.domain.Category;


@Repository
public interface AddressRepository extends JpaRepository<Address, Integer> {

}
