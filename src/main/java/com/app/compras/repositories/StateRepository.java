package com.app.compras.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.compras.domain.State;


@Repository
public interface StateRepository extends JpaRepository<State, Integer> {

}
