package com.app.compras.vo;


import com.app.compras.domain.Client;
import com.app.compras.services.validation.ClientUpdate;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@ClientUpdate
public class ClientVO implements Serializable {

    private Integer id;

    @NotEmpty( message = "Campo Obrigatório." )
    @Length( min = 5, max = 120, message = "O tamanho deve ser entre 5 e 120 caracteres." )
    private String name;

    @NotEmpty( message = "Campo Obrigatório." )
    @Email( message = "Email inválido")
    private String email;


    public ClientVO( Integer id, String name, String email ) {

        this.id = id;
        this.name = name;
        this.email = email;
    }


    public ClientVO() {

    }


    public ClientVO( Client client ) {

        this.id = client.getId();
        this.name = client.getName();
        this.email = client.getEmail();
    }


    public Integer getId() {

        return id;
    }


    public void setId( Integer id ) {

        this.id = id;
    }


    public String getName() {

        return name;
    }


    public void setName( String name ) {

        this.name = name;
    }


    public String getEmail() {

        return email;
    }


    public void setEmail( String email ) {

        this.email = email;
    }
}
