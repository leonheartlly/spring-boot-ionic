package com.app.compras.vo;


import com.app.compras.domain.Category;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;


public class CategoryVO implements Serializable {

    private Integer id;

    @NotEmpty(message="Preenchimento obrigatório.")
    @Length(min=5, max=80, message = "O tamanho deve ser entre 5 e 80 caracteres")
    private String name;


    public CategoryVO() {

    }


    public CategoryVO( Category category ) {

        id = category.getId();
        name = category.getName();
    }


    public Integer getId() {

        return id;
    }


    public void setId( Integer id ) {

        this.id = id;
    }


    public String getName() {

        return name;
    }


    public void setName( String name ) {

        this.name = name;
    }
}
