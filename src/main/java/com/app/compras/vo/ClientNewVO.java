package com.app.compras.vo;


import com.app.compras.services.validation.ClientInsert;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@ClientInsert
public class ClientNewVO implements Serializable {

    @NotEmpty( message = "Campo Obrigatório." )
    @Length( min = 5, max = 120, message = "O tamanho deve ser entre 5 e 120 caracteres." )
    private String name;

    @NotEmpty( message = "Campo Obrigatório." )
    @Email( message = "Email inválido" )
    private String email;

    @NotEmpty( message = "Campo Obrigatório." )
    private String cpfcnpj;

    private Integer clientType;

    @NotEmpty( message = "Campo Obrigatório." )
    private String street;

    @NotEmpty( message = "Campo Obrigatório." )
    private String number;

    private String complement;

    private String district;

    @NotEmpty( message = "Campo Obrigatório." )
    private String cep;

    private String fone1;

    private String fone2;

    private String fone3;

    private Integer cityId;


    public ClientNewVO() {

    }


    public String getName() {

        return name;
    }


    public void setName( String name ) {

        this.name = name;
    }


    public String getEmail() {

        return email;
    }


    public void setEmail( String email ) {

        this.email = email;
    }


    public String getCpfcnpj() {

        return cpfcnpj;
    }


    public void setCpfcnpj( String cpfcnpj ) {

        this.cpfcnpj = cpfcnpj;
    }


    public Integer getClientType() {

        return clientType;
    }


    public void setClientType( Integer clientType ) {

        this.clientType = clientType;
    }


    public String getStreet() {

        return street;
    }


    public void setStreet( String street ) {

        this.street = street;
    }


    public String getNumber() {

        return number;
    }


    public void setNumber( String number ) {

        this.number = number;
    }


    public String getComplement() {

        return complement;
    }


    public void setComplement( String complement ) {

        this.complement = complement;
    }


    public String getDistrict() {

        return district;
    }


    public void setDistrict( String district ) {

        this.district = district;
    }


    public String getCep() {

        return cep;
    }


    public void setCep( String cep ) {

        this.cep = cep;
    }


    public String getFone1() {

        return fone1;
    }


    public void setFone1( String fone1 ) {

        this.fone1 = fone1;
    }


    public String getFone2() {

        return fone2;
    }


    public void setFone2( String fone2 ) {

        this.fone2 = fone2;
    }


    public String getFone3() {

        return fone3;
    }


    public void setFone3( String fone3 ) {

        this.fone3 = fone3;
    }


    public Integer getCityId() {

        return cityId;
    }


    public void setCityId( Integer cityId ) {

        this.cityId = cityId;
    }
}
