package com.app.compras.services;


import java.util.List;
import java.util.Optional;

import com.app.compras.domain.Order;
import com.app.compras.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.compras.domain.Category;
import com.app.compras.repositories.CategoryRepository;
import com.app.compras.services.exceptions.ObjectNotFoundException;


@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;


    public Order findById( Integer id ) {

        Optional< Order > order = orderRepository.findById( id );

        return order.orElseThrow( () -> new ObjectNotFoundException( "Objeto nao encontrado. Id: " + id + ", Tipo: " + Order.class.getName() ) );

    }


    public List< Order > listAll() {

        List< Order > orders = orderRepository.findAll();
        return orders;
    }
}
