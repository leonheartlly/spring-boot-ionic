package com.app.compras.services.validation;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.app.compras.domain.Client;
import com.app.compras.domain.enuns.ClientType;
import com.app.compras.repositories.ClientRepository;
import com.app.compras.resources.exception.FieldMessage;
import com.app.compras.services.validation.utils.CPFCNPJValidator;
import com.app.compras.vo.ClientVO;
import org.springframework.web.servlet.HandlerMapping;


public class ClientUpdateValidator implements ConstraintValidator< ClientUpdate, ClientVO > {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private HttpServletRequest httpServletRequest;


    @Override
    public void initialize( ClientUpdate ann ) {

    }


    @Override
    public boolean isValid( ClientVO objVO, ConstraintValidatorContext context ) {

        // obtendo ID do cliente passado na URI para atualizar o cliente
        Map< String, String > map = (Map< String, String >) httpServletRequest.getAttribute( HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE );
        Integer uriId = Integer.parseInt( map.get( "id" ) );

        List< FieldMessage > list = new ArrayList<>();

        Client aux = clientRepository.findByEmail( objVO.getEmail() );
        if ( aux != null && aux.getId().equals( uriId ) ) {
            list.add( new FieldMessage( "email", "Email já existente na base de dados." ) );
        }

        for ( FieldMessage e : list ) {
            // pega cada erro detectado e insere no framework para ser tratado na exception
            // criada metodo validation classe ResourceExcepionHandler
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate( e.getMessage() ).addPropertyNode( e.getFieldName() ).addConstraintViolation();
        }
        // Se a lista estiver vazia, retorna verdadeiro, do contrário retorna falso
        return list.isEmpty();
    }
}
