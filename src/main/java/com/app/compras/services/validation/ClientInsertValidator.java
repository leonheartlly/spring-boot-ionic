package com.app.compras.services.validation;


import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.app.compras.domain.Client;
import com.app.compras.domain.enuns.ClientType;
import com.app.compras.repositories.ClientRepository;
import com.app.compras.resources.exception.FieldMessage;
import com.app.compras.services.validation.utils.CPFCNPJValidator;
import com.app.compras.vo.ClientNewVO;
import org.springframework.beans.factory.annotation.Autowired;


public class ClientInsertValidator implements ConstraintValidator< ClientInsert, ClientNewVO > {

    @Autowired
    private ClientRepository clientRepository;


    @Override
    public void initialize( ClientInsert ann ) {

    }


    @Override
    public boolean isValid( ClientNewVO objVO, ConstraintValidatorContext context ) {

        List< FieldMessage > list = new ArrayList<>();

        if ( objVO.getClientType().equals( ClientType.PESSOA_FISICA.getCod() ) && !CPFCNPJValidator.isValidCPF( objVO.getCpfcnpj() ) ) {
            list.add( new FieldMessage( "cpfcnpj", "CPF inválido." ) );
        }

        if ( objVO.getClientType().equals( ClientType.PESSOA_JURIDICA.getCod() ) && !CPFCNPJValidator.isValidCNPJ( objVO.getCpfcnpj() ) ) {
            list.add( new FieldMessage( "cpfcnpj", "CNPJ inválido." ) );
        }

        Client aux = clientRepository.findByEmail( objVO.getEmail() );
        if ( aux != null ) {
            list.add( new FieldMessage( "email", "Email já existente na base de dados." ) );
        }

        for ( FieldMessage e : list ) {
            // pega cada erro detectado e insere no framework para ser tratado na exception
            // criada metodo validation classe ResourceExcepionHandler
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate( e.getMessage() ).addPropertyNode( e.getFieldName() ).addConstraintViolation();
        }
        // Se a lista estiver vazia, retorna verdadeiro, do contrário retorna falso
        return list.isEmpty();
    }
}
