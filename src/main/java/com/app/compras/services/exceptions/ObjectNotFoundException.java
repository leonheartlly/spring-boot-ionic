package com.app.compras.services.exceptions;


public class ObjectNotFoundException extends RuntimeException {

    /**
     * Recebe mensagem de excessão.
     * 
     * @param msg
     */
    public ObjectNotFoundException( String msg ) {

        super( msg );

    }


    /**
     * Recebe a mensagem da excessão e uma causa ocorrida antes e lançada.
     * 
     * @param msg
     *            mensagem da excessão.
     * @param cause
     *            Causa lançada recebida.
     */
    public ObjectNotFoundException( String msg, Throwable cause ) {

        super( msg, cause );
    }

}
