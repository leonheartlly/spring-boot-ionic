package com.app.compras.services;


import com.app.compras.domain.Address;
import com.app.compras.domain.BilletPayment;
import com.app.compras.domain.CardPayment;
import com.app.compras.domain.Category;
import com.app.compras.domain.City;
import com.app.compras.domain.Client;
import com.app.compras.domain.Order;
import com.app.compras.domain.OrderItem;
import com.app.compras.domain.Payment;
import com.app.compras.domain.Product;
import com.app.compras.domain.State;
import com.app.compras.domain.enuns.ClientType;
import com.app.compras.domain.enuns.PaymentState;
import com.app.compras.repositories.AddressRepository;
import com.app.compras.repositories.CategoryRepository;
import com.app.compras.repositories.CityRepository;
import com.app.compras.repositories.ClientRepository;
import com.app.compras.repositories.OrderItemRepository;
import com.app.compras.repositories.OrderRepository;
import com.app.compras.repositories.PaymentRepository;
import com.app.compras.repositories.ProductRepository;
import com.app.compras.repositories.StateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;


@Service
public class DBService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private StateRepository stateRepository;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;


    public void instantiateDevDataBase()
        throws ParseException {

        Category cat1 = new Category( null, "Informática" );
        Category cat2 = new Category( null, "Escritório" );
        Category cat3 = new Category( null, "Loucura" );
        Category cat4 = new Category( null, "Estado" );
        Category cat5 = new Category( null, "Yogurte" );
        Category cat6 = new Category( null, "Inviolável" );
        Category cat7 = new Category( null, "Tretas" );

        Product p1 = new Product( null, "Mouse", 10.35D );
        Product p2 = new Product( null, "Computador", 2000.0D );
        Product p3 = new Product( null, "Impressora", 350.0D );
        Product p4 = new Product( null, "Mesa de Escritório", 300.0D );
        Product p5 = new Product( null, "Toalha", 50.0D );
        Product p6 = new Product( null, "Colcha", 200.0D );
        Product p7 = new Product( null, "TV True Color", 1200.0D );
        Product p8 = new Product( null, "Roçadeira", 800.0D );
        Product p9 = new Product( null, "Abajur", 350.0D );
        Product p10 = new Product( null, "Pendente", 180.0D );
        Product p11 = new Product( null, "Shampoo", 350.0D );

        cat1.getProducts().addAll( Arrays.asList( p1, p2, p3 ) );
        cat2.getProducts().addAll( Arrays.asList( p2, p4 ) );
        cat3.getProducts().addAll( Arrays.asList( p5, p6 ) );
        cat4.getProducts().addAll( Arrays.asList( p1, p2, p3, p7 ) );
        cat5.getProducts().addAll( Arrays.asList( p8 ) );
        cat6.getProducts().addAll( Arrays.asList( p9, p10 ) );
        cat7.getProducts().addAll( Arrays.asList( p11 ) );

        p1.getCategories().addAll( Arrays.asList( cat1, cat4 ) );
        p2.getCategories().addAll( Arrays.asList( cat1, cat2, cat4 ) );
        p3.getCategories().addAll( Arrays.asList( cat1, cat4 ) );
        p4.getCategories().addAll( Arrays.asList( cat2 ) );
        p5.getCategories().addAll( Arrays.asList( cat3) );
        p6.getCategories().addAll( Arrays.asList( cat3) );
        p7.getCategories().addAll( Arrays.asList( cat4 ) );
        p8.getCategories().addAll( Arrays.asList( cat5) );
        p9.getCategories().addAll( Arrays.asList( cat6 ) );
        p10.getCategories().addAll( Arrays.asList( cat6 ) );
        p11.getCategories().addAll( Arrays.asList( cat7 ) );

        State st = new State( null, "Minas Gerais" );
        State st2 = new State( null, "São Paulo" );

        City ct = new City( null, "Uberlandia", st );
        City ct2 = new City( null, "São paulo", st2 );
        City ct3 = new City( null, "Campinas", st2 );

        st.getCities().addAll( Arrays.asList( ct ) );
        st.getCities().addAll( Arrays.asList( ct2, ct3 ) );

        Client cli = new Client( null, "Maria das Dores", "maria@gmail.com", "08559205640", ClientType.PESSOA_FISICA );
        cli.getPhones().addAll( Arrays.asList( "465456456", "465456456" ) );

        Address ad = new Address( null, "rua flores", "10", "portão rosa", "Dos Infernos", "38408170", cli, ct );
        Address ad2 = new Address( null, "dos caraios", "100", "portão dos pretos", "florzinha", "38408170", cli, ct2 );

        cli.getAddresses().addAll( Arrays.asList( ad, ad2 ) );

        SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy HH:mm" );

        Order or = new Order( null, sdf.parse( "30/09/2017 10:32" ), cli, ad );
        Order or2 = new Order( null, sdf.parse( "10/10/2017 18:35" ), cli, ad2 );

        Payment pay = new CardPayment( null, PaymentState.QUITADO, or, 6 );
        or.setPayment( pay );

        Payment pay2 = new BilletPayment( null, PaymentState.PENDENTE, or2, sdf.parse( "20/10/2017 00:00" ), null );
        or2.setPayment( pay2 );

        cli.getOrders().addAll( Arrays.asList( or, or2 ) );

        OrderItem ori = new OrderItem( or, p1, 0.00, 2, 10.35D );
        OrderItem ori1 = new OrderItem( or, p2, 0.00, 1, 2000.0D );
        OrderItem ori2 = new OrderItem( or2, p3, 100.00, 1, 350.0D );

        or.getItens().addAll( Arrays.asList( ori ) );
        or.getItens().add( ori1 );
        or2.getItens().addAll( Arrays.asList( ori2 ) );

        p1.getItens().addAll( Arrays.asList( ori ) );
        p2.getItens().addAll( Arrays.asList( ori1 ) );
        p3.getItens().addAll( Arrays.asList( ori2 ) );

        categoryRepository.saveAll( Arrays.asList( cat1, cat2, cat3, cat4, cat5, cat6, cat7 ) );

        productRepository.saveAll( Arrays.asList( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11 ) );

        stateRepository.saveAll( Arrays.asList( st, st2 ) );

        cityRepository.saveAll( Arrays.asList( ct, ct2, ct3 ) );

        clientRepository.saveAll( Arrays.asList( cli ) );

        addressRepository.saveAll( Arrays.asList( ad, ad2 ) );

        orderRepository.saveAll( Arrays.asList( or, or2 ) );

        paymentRepository.saveAll( Arrays.asList( pay, pay2 ) );

        orderItemRepository.saveAll( Arrays.asList( ori, ori1, ori2 ) );
    }
}
