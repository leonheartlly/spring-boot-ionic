package com.app.compras.services;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.app.compras.domain.Category;
import com.app.compras.repositories.CategoryRepository;
import com.app.compras.services.exceptions.DataIntegrityException;
import com.app.compras.services.exceptions.ObjectNotFoundException;
import com.app.compras.vo.CategoryVO;


@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;


    public Category findById( Integer id ) {

        Optional< Category > category = categoryRepository.findById( id );

        return category.orElseThrow( () -> new ObjectNotFoundException( "Objeto nao encontrado. Id: " + id + ", Tipo: " + Category.class.getName() ) );

    }


    public List< Category > listAll() {

        List< Category > categories = categoryRepository.findAll();
        return categories;
    }


    public Category insert( Category category ) {

        category.setId( null ); // garantindo que é um objeto novo
        return categoryRepository.save( category );

    }


    public Category update( Category category ) {

        Category oldCategory = findById( category.getId() );

        updateData( oldCategory, category );
        return categoryRepository.save( oldCategory );
    }


    private void updateData( Category oldCategory, Category newCategory ) {

        oldCategory.setName( newCategory.getName() );

    }


    public void delete( Integer id ) {

        findById( id );
        try {

            categoryRepository.deleteById( id );
        } catch ( DataIntegrityViolationException dive ) {
            throw new DataIntegrityException( "You cannot delete a Category with child Products" );
        }
    }


    /***
     * Busca paginada de categorias.
     * 
     * @param page
     *            pagina da busca.
     * @param linesPerPage
     *            quantidade de linhas.
     * @param orderBy
     *            ordenação.
     * @param direction
     *            direção.
     * @return
     */
    public Page< Category > findPage( Integer page, Integer linesPerPage, String orderBy, String direction ) {

        PageRequest pageRequest = PageRequest.of( page, linesPerPage, Sort.Direction.valueOf( direction ), orderBy );
        return categoryRepository.findAll( pageRequest );
    }


    public Category convertVO( CategoryVO categoryVO ) {

        return new Category( categoryVO.getId(), categoryVO.getName() );

    }
}
