package com.app.compras.services;


import java.util.List;
import java.util.Optional;

import com.app.compras.domain.Category;
import com.app.compras.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.app.compras.domain.Product;
import com.app.compras.repositories.ProductRepository;
import com.app.compras.services.exceptions.ObjectNotFoundException;


@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;


    public Product findById( Integer id ) {

        Optional< Product > product = productRepository.findById( id );

        return product.orElseThrow( () -> new ObjectNotFoundException( "Objeto nao encontrado. Id: " + id + ", Tipo: " + Product.class.getName() ) );

    }


    public List< Product > listAll() {

        List< Product > products = productRepository.findAll();
        return products;
    }


    public Page< Product > search( String name, List< Integer > ids, Integer page, Integer linesPerPage, String orderBy, String direction ) {

        PageRequest pageRequest = PageRequest.of( page, linesPerPage, Sort.Direction.valueOf( direction ), orderBy );
        List<Category> categories = categoryRepository.findAllById( ids );

        return productRepository.findDistinctByNameContainingAndCategoriesIn( name, categories, pageRequest );
    }
}
