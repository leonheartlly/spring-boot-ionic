package com.app.compras.services;


import java.util.List;
import java.util.Optional;

import com.app.compras.domain.Address;
import com.app.compras.domain.City;
import com.app.compras.domain.enuns.ClientType;
import com.app.compras.repositories.AddressRepository;
import com.app.compras.repositories.ClientRepository;
import com.app.compras.services.exceptions.DataIntegrityException;
import com.app.compras.vo.ClientNewVO;
import com.app.compras.vo.ClientVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.app.compras.domain.Client;
import com.app.compras.domain.Client;
import com.app.compras.repositories.ClientRepository;
import com.app.compras.services.exceptions.ObjectNotFoundException;

import javax.transaction.Transactional;


@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private AddressRepository addressRepository;


    public Client findById( Integer id ) {

        Optional< Client > client = clientRepository.findById( id );

        return client.orElseThrow( () -> new ObjectNotFoundException( "Objeto nao encontrado. Id: " + id + ", Tipo: " + Client.class.getName() ) );

    }


    public List< Client > listAll() {

        List< Client > clients = clientRepository.findAll();
        return clients;
    }


    @Transactional // garante que a inserção ocorra de forma transacional
    public Client insert( Client client ) {

        client.setId( null ); // garantindo que é um objeto novo
        client = clientRepository.save( client );
        addressRepository.saveAll( client.getAddresses() );
        return client;

    }


    public Client update( Client client ) {

        Client oldClient = findById( client.getId() );

        updateData( oldClient, client );
        return clientRepository.save( oldClient );
    }


    private void updateData( Client oldClient, Client newClient ) {

        oldClient.setName( newClient.getName() );
        oldClient.setEmail( newClient.getEmail() );

    }


    public void delete( Integer id ) {

        findById( id );
        try {

            clientRepository.deleteById( id );
        } catch ( DataIntegrityViolationException dive ) {
            throw new DataIntegrityException( "You cannot delete a Client with related entities." );
        }
    }


    /***
     * Busca paginada de categorias.
     *
     * @param page
     *            pagina da busca.
     * @param linesPerPage
     *            quantidade de linhas.
     * @param orderBy
     *            ordenação.
     * @param direction
     *            direção.
     * @return
     */
    public Page< Client > findPage( Integer page, Integer linesPerPage, String orderBy, String direction ) {

        PageRequest pageRequest = PageRequest.of( page, linesPerPage, Sort.Direction.valueOf( direction ), orderBy );
        return clientRepository.findAll( pageRequest );
    }


    public Client convertVO( ClientVO clientVO ) {

        return new Client( clientVO.getId(), clientVO.getName(), clientVO.getEmail(), null, null );

    }


    public Client convertVO( ClientNewVO clientNewVO ) {

        Client cli = new Client( null, clientNewVO.getName(), clientNewVO.getEmail(), clientNewVO.getCpfcnpj(),
            ClientType.getClientType( clientNewVO.getClientType() ) );
        City city = new City( clientNewVO.getCityId(), null, null );
        Address address = new Address( null, clientNewVO.getStreet(), clientNewVO.getNumber(), clientNewVO.getComplement(), clientNewVO.getDistrict(),
            clientNewVO.getCep(), cli, city );

        cli.getAddresses().add( address );
        cli.getPhones().add( clientNewVO.getFone1() );

        if ( clientNewVO.getFone2() != null ) {
            cli.getPhones().add( clientNewVO.getFone2() );
        }
        if ( clientNewVO.getFone3() != null ) {
            cli.getPhones().add( clientNewVO.getFone3() );
        }

        return cli;

    }

}
