package com.app.compras;


import com.app.compras.domain.Address;
import com.app.compras.domain.BilletPayment;
import com.app.compras.domain.CardPayment;
import com.app.compras.domain.Category;
import com.app.compras.domain.City;
import com.app.compras.domain.Client;
import com.app.compras.domain.Order;
import com.app.compras.domain.OrderItem;
import com.app.compras.domain.Payment;
import com.app.compras.domain.Product;
import com.app.compras.domain.State;
import com.app.compras.domain.enuns.ClientType;
import com.app.compras.domain.enuns.PaymentState;
import com.app.compras.repositories.AddressRepository;
import com.app.compras.repositories.CategoryRepository;
import com.app.compras.repositories.CityRepository;
import com.app.compras.repositories.ClientRepository;
import com.app.compras.repositories.OrderItemRepository;
import com.app.compras.repositories.OrderRepository;
import com.app.compras.repositories.PaymentRepository;
import com.app.compras.repositories.ProductRepository;
import com.app.compras.repositories.StateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.text.SimpleDateFormat;
import java.util.Arrays;


@SpringBootApplication
public class ComprasSpringApplication implements CommandLineRunner {




    public static void main( String[] args ) {

        SpringApplication.run( ComprasSpringApplication.class, args );
    }


    /**
     * Criado apenas para adicionar objetos toda vez que subir o banco e facilitar
     * os testes
     * 
     * @param args
     * @throws Exception
     */
    @Override
    public void run( String... args )
        throws Exception {



    }
}
